#! /bin/sh

set -eux

supervisord -c /etc/dd-agent/supervisor.conf
cd /var/log/datadog
touch app.log collector.log forwarder.log supervisord.log
tail -f *.log
